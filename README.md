# PSOM/CHOP Image Processing Workshop

Repository for notebooks and sample data for image processing workshop.

## Installing Python and Requirements

Open the file [Installing_Python.md](Installing_Python.md) for instructions on installing Python and setting up VS Code/Jupyter Notebooks.

